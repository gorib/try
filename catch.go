package try

import "fmt"

func Catch(try func(), catch ...func(throwable error)) {
	defer func() {
		if exception := recover(); exception != nil {
			if len(catch) > 0 && catch[0] != nil {
				var err error
				switch e := exception.(type) {
				case error:
					err = e
				default:
					err = fmt.Errorf("%v", e)
				}
				catch[0](err)
			}
		}
	}()

	try()
}

func Throw[T any](result T, err error) T {
	ThrowError(err)
	return result
}

func ThrowError(err error) {
	if err != nil {
		panic(err)
	}
}
